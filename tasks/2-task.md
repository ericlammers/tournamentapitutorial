# Task One
Introduces sql datbases and docker. 

## Steps

Use docker to start and work with postgres:
1. Download docker (if not already installed) and docker compose
2. Review the docker compose file. Note that it is mapping ports (making a port on your local computer map to a port with the docker container) and setting environment variables in the containers (these are predefined by the docker image and help configure it) we are also volume mounting a sql file that will be used to create the datbase (volume mounting means essentially copying the file into the container).
3. From the terminal move into `dev/local-setup` and run `docker-compose up`
4. From a new terminal run `docker ps` to list the running containers. Notice that there is a postgres container (running the database) and a pgadmin container (running the UI). These are tiny isolate environments built to run a single thing! No need to install docker or pgadmin on your computer. Just install docker and you can run all kinds of containers running all kinds of things by just running the containers.
5. Open up `http://localhost:5050` to view pgadmin. Check the compose file to find the username and password for pgadmin. Once logged in left click servers, then click Register => Server, and give it a name then for connection use: `database` as the url (this is the url setup by the docker compose env) use `user` as the username and `password` as the password.
6. Run a query by opening the query tool and running `SELECT * FROM "Tournament"` then try adding a few tournament using sql and possibly deleting one. In general I have found few jobs require db expertise but knowing some basic SQL is definitel important!
7. Now that you have some hands on experience with docker this video to learn a bit more about it: https://www.youtube.com/watch?v=pTFZFxd4hOI&ab_channel=ProgrammingwithMosh or if you are feeling ambishes this one: https://www.youtube.com/watch?v=3c-iBn73dDE&ab_channel=TechWorldwithNana (I like both these people Nana has some great devops videos)
   
Use entity core ORM to access the data:
8. I have added a new repository implementation called EntityCoreTournamentRepository. This repository connects to the database that you started using docker compose. ORMs (object relation mappers - I think it stands for...) are frameworks for working with databases that take care of some of the more low level details. You don't need to write the sql yourself. Instead you work with there interfaces which is like writing more or less regular code. You can read about Entity core a bit looking at the microsoft docs here: https://learn.microsoft.com/en-us/ef/core/ 
9. Start the app and run the system tests. Notice that they fail now because not all the methods for the repository are properly implemented. Implement those methods using the entity core and then re-run the tests to see them passing!

Couple things to note:
- I have added a entity object type that is very similar to the Tournament domain object, this adds some boiler plate code but I do this to decouple the domain objects from the database objects, this means we don't need to annotate our domain objects with entity core annotations and we also have flexibility to have the db object structure look slightly different then the domain objects structure
- I use an extension method to map from entity core object to domain object, this is a cool C# feature that lets you add methods to a class without updating the class source file
