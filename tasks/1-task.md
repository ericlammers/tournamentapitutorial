# Task One
Introduces the project structure and restful APIs. Also has you running some API requests and adding a few endpoints of your own. You also add a few simple endpoints of your own and update the system tests to verify they work as you expect.

## Steps

1. Download the latest version of .net core for mac: `https://dotnet.microsoft.com/en-us/download`
2. Run the TournamentAPI. From in the TournamentAPI folder in a terminal run `dotnet run`
```
Building...
info: Microsoft.Hosting.Lifetime[14]
      Now listening on: http://localhost:5259
info: Microsoft.Hosting.Lifetime[0]
      Application started. Press Ctrl+C to shut down.
info: Microsoft.Hosting.Lifetime[0]
      Hosting environment: Development
info: Microsoft.Hosting.Lifetime[0]
      Content root path: /Users/ericlammers/dev/courses/tournament-api/TournamentAPI
```
3. Hit it with a curl request: `curl http://localhost:5259/tournament/bd93d113-c179-4437-ac87-acb2da35224a`
```
{"name":"Tournament One","id":"bd93d113-c179-4437-ac87-acb2da35224a"}
```
4. Read the [Project Structure document](../docs/concepts/project-structure.md) and review the project code 
5. Read the [Restful APIs document](../docs/concepts/restful-apis.md)
6. [Download Postman](https://www.postman.com/downloads/) and set up the curl request from step 3 in Postman
7. Run the system test. With the API still running in a new terminal move into the SystemTest folder and run: `dotnet test`
```
Passed!  - Failed:     0, Passed:     1, Skipped:     0, Total:     1, Duration: < 1 ms - SystemTests.dll (net7.0)
```
8. Create your own git branch 
   - the plan will be to have your own working branch for each task and then create a merge request that I can review before merging the changes into the main branch
   - git checkout -b jolene-task-one
9.  Add endpoints for creating and deleting a tournament 
   - Will require updates to the repository, service and controller
     -  Creates request body should have only the tournament name `{ "name": "New Tournament" }`
     -  The id should be generated in the repository
     -  The response for create should include the id
  -  The microsoft docs are always pretty solid, [their stuff on controllers](https://learn.microsoft.com/en-us/aspnet/core/web-api/?view=aspnetcore-7.0) could be helpful here 
   - For the controller pay attention to the http method types 
   - Add methods to your postman collection to test them out
10.  Update the system test to verify the new endpoints
    - Something like create a tournament, retrieve the tournament, delete the tournament, retrieve again and verify it's a 404
11.  Create an MR in gitlab
