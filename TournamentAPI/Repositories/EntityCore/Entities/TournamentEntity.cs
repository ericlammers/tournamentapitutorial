using System.ComponentModel.DataAnnotations.Schema;

namespace TournamentAPI.Repository.EntityCore.Entities;

public class TournamentEntity
{
    [Column("name")]
    public string? Name { get; set; }

    [Column("id")]
    public string? Id { get; set; }
}