using TournamentAPI.Domain;

namespace TournamentAPI.Repository.EntityCore.Entities;

public static class EntityMapperExtensions
{
        public static Tournament ToDomainObject(this TournamentEntity tournamentEntity)
        {
            return new Tournament { Name = tournamentEntity.Name, Id = tournamentEntity.Id };
        }
}