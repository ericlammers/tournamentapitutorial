using Microsoft.EntityFrameworkCore;
using TournamentAPI.Repository.EntityCore.Entities;

namespace TournamentAPI.Repository.EntityCore;

public class TournamentContext : DbContext
{
    public DbSet<TournamentEntity> Tournament { get; set; }

    protected readonly IConfiguration Configuration;

    public TournamentContext(DbContextOptions<TournamentContext> options) : base(options)
    {
    }
}