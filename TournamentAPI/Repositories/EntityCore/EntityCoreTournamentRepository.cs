
using TournamentAPI.Domain;
using TournamentAPI.Repository.EntityCore.Entities;

namespace TournamentAPI.Repository.EntityCore;

public class EntityCoreTournamentRepository : ITournamentRepository
{
    private TournamentContext dataContext;

    private ILogger<EntityCoreTournamentRepository> logger;

    public EntityCoreTournamentRepository(ILogger<EntityCoreTournamentRepository> logger, TournamentContext dataContext)
    {
        this.logger = logger;
        this.dataContext = dataContext;
    }

    public Tournament? Get(string id)
    {
        logger.LogDebug($"Getting tournament {id}");
        return dataContext.Tournament.OrderBy(tournament => tournament.Id).First().ToDomainObject();
    }

    public Tournament Create(Tournament tournament) {
        throw new NotImplementedException();
    }

    public bool TournamentExists(string id) {
        throw new NotImplementedException();
    }

    public void Delete(string id) {
        throw new NotImplementedException();
    }
}