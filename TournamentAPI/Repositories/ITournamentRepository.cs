
using Microsoft.Extensions.Options;
using TournamentAPI.Domain;

namespace TournamentAPI.Repository;

public interface ITournamentRepository
{
    public Tournament? Get(string id);
    public Tournament Create(Tournament tournament);
    public bool TournamentExists(string id);
    public void Delete(string id);
    
}