
using Microsoft.AspNetCore.Http.HttpResults;
using TournamentAPI.Domain;

namespace TournamentAPI.Repository.InMemory;

public class InMemoryTournamentRepository : ITournamentRepository
{
    private List<Tournament> tournaments;
    private ILogger<InMemoryTournamentRepository> logger;

    public InMemoryTournamentRepository(ILogger<InMemoryTournamentRepository> logger)
    {
        this.logger = logger;
        this.tournaments = CreateInitialTournaments();
    }

    public Tournament? Get(string id)
    {
        logger.LogDebug($"Getting tournament {id}");
        return tournaments.Find(tournament => tournament.Id == id);
    }

    public Tournament Create(Tournament tournament)
    {
        logger.LogDebug($"Posting tournament {tournament.Name}");
        tournament.Id = GenerateNewId();
        tournaments.Add(tournament);
        return tournament;
    }

    public bool TournamentExists(string id)
    {
        logger.LogDebug($"Checking if tournament {id} exists");
        var tournament = tournaments.Find(tournament => tournament.Id == id);
        if (tournament == null)
        {
            return false;
        }
        return true;
    }
    
    public void Delete(string id)
    {
        logger.LogDebug($"Deleting tournament {id}");
        var tournamentToRemove = tournaments.Single(tournament => tournament.Id == id);
        var isDeleted = tournaments.Remove(tournamentToRemove);

        if(!isDeleted) {
            throw new Exception();
        }

    }
    
    private string GenerateNewId() {
        return Guid.NewGuid().ToString();
    }
    
    
    private List<Tournament> CreateInitialTournaments() {
        var tournaments = new List<Tournament>();
        tournaments.Add(new Tournament { Name = "Tournament One", Id = "bd93d113-c179-4437-ac87-acb2da35224a"});
        return tournaments;
    }
}