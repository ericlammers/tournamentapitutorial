using Microsoft.AspNetCore.Mvc;
using TournamentAPI.Domain;
using TournamentAPI.Service;

namespace TournamentAPI.Controllers;

[ApiController]
[Route("[controller]")] // Sets up all routes for this class to start with /tournament
public class TournamentController : ControllerBase
{
    private ITournamentService tournamentService;

    private ILogger<TournamentController> logger;

    public TournamentController(ITournamentService tournamentService, ILogger<TournamentController> logger)
    {
        this.tournamentService = tournamentService;
        this.logger = logger;
    }

    [HttpGet("{id}")] // The endpoint here is just the id, so it's base path + id for example /tournament/bd93d113-c179-4437-ac87-acb2da35224a
    public ActionResult<Tournament> Get(string id)
    {
        logger.LogInformation($"Received a request to retrieve tournament {id}");

        var tournament = tournamentService.GetTournament(id);

        if(tournament == null) {
            logger.LogInformation($"Tournament {id} was not found.");
            return NotFound();
        }

        return Ok(tournament);
    }

    [HttpPost] 
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult<Tournament> Create(Tournament tournament)
    {
        logger.LogInformation($"Received a request to create a new tournament");

        var tournamentResult = tournamentService.CreateTournament(tournament);
        
        if(tournamentResult == null) {
            logger.LogInformation($"Tournament was not able to be created.");
            return BadRequest();
        }

        return Ok(tournament);
    }
    
    [HttpDelete("{id}")]
    public ActionResult<Tournament> Delete(string id)
    {
        
        if(!tournamentService.TournamentExists(id)) {
            return NotFound();
        }

        try
        {
            tournamentService.DeleteTournament(id);
        }
        catch(Exception e)
        {
            logger.LogWarning(e.Message);
            return StatusCode(StatusCodes.Status500InternalServerError); // 500 error code
        }
        
        return Ok();
    }
}
