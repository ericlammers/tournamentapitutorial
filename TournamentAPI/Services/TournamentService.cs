
using TournamentAPI.Domain;
using TournamentAPI.Repository;

namespace TournamentAPI.Service;

public class TournamentService : ITournamentService
{
    private ITournamentRepository tournamentRepository;

    private ILogger<TournamentService> logger;

    public TournamentService(ITournamentRepository tournamentRepository, ILogger<TournamentService> logger)
    {
        this.tournamentRepository = tournamentRepository;
        this.logger = logger;
    }

    public Tournament? GetTournament(string id)
    {
        logger.LogInformation($"Getting tournament {id}");
        return tournamentRepository.Get(id);
    }

    public Tournament CreateTournament(Tournament tournament)
    {
        logger.LogInformation($"Creating tournament {tournament.Id}");
        return tournamentRepository.Create(tournament);
    }

    public bool TournamentExists(string id)
    {
        logger.LogInformation($"Checking if Tournament {id} Exists");
        return tournamentRepository.TournamentExists(id);

    }
    public void DeleteTournament(string id)
    {
        logger.LogInformation($"Deleting tournament {id}");
        tournamentRepository.Delete(id);
    }
}