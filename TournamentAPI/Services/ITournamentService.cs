
using TournamentAPI.Domain;

namespace TournamentAPI.Service;

public interface ITournamentService
{
    public Tournament? GetTournament(string id);
    public Tournament CreateTournament(Tournament tournament);
    public bool TournamentExists(string id);
    public void DeleteTournament(string id);
}