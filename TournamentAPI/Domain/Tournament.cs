namespace TournamentAPI.Domain;

public class Tournament
{
    public string? Name { get; set; }

    public string? Id { get; set; }
}