using System.Text;
using System.Text.Json;
using Newtonsoft.Json;
using TournamentAPI.Domain;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace SystemTests;

public class TournamentTest
{
    private string baseAPIUrl = "http://localhost:5259";

    private HttpClient httpClient = new HttpClient();
    private JsonSerializerOptions options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

    [Fact]
    public async void TestSetupTournamentAsync()
    {
        // For now we just retrieve the pre-existing tournament that is defined in InMemoryTournamentRepository
        // Once create and delete endpoints are added we should create, get, delete, confirm deletion here

        var client = new HttpClient();

        using HttpResponseMessage tournamentResponse = await client.GetAsync(CreateApiUrl("/tournament/bd93d113-c179-4437-ac87-acb2da35224a"));

        //Create a tournament
        Tournament tournament1 = new Tournament { Name = "Tournament One" };
        var json = JsonConvert.SerializeObject(tournament1);
        StringContent stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json"); // use MediaTypeNames.Application.Json in Core 3.0+ and Standard 2.1+
        using HttpResponseMessage createTournamentResponse = await client.PostAsync(CreateApiUrl("/tournament/"), stringContent);
       
        //Return a 200, with the created tournament in the body
        Assert.Equal(System.Net.HttpStatusCode.OK, createTournamentResponse.StatusCode);
        string responseBody = await createTournamentResponse.Content.ReadAsStringAsync();
        var tournament = JsonSerializer.Deserialize<Tournament>(responseBody, options);
        Assert.NotNull(tournament);
        Assert.NotNull(tournament.Name);
        Assert.NotNull(tournament.Id);
        Assert.Equal(tournament1.Name, tournament.Name);
        
        //Get tournament
        using HttpResponseMessage getTournamentResponse = await client.GetAsync(CreateApiUrl($"/tournament/{tournament.Id}"));
        
        // Returned a 200, containing the tournament we expect in the body
        Assert.Equal(System.Net.HttpStatusCode.OK, getTournamentResponse.StatusCode);
        responseBody = await getTournamentResponse.Content.ReadAsStringAsync();
        tournament = JsonSerializer.Deserialize<Tournament>(responseBody, options);
        Assert.NotNull(tournament);
        Assert.NotNull(tournament.Name);
        Assert.NotNull(tournament.Id);
        Assert.Equal(tournament1.Name, tournament.Name);
        
        //Delete tournament
        using HttpResponseMessage deleteTournamentResponse = await client.DeleteAsync(CreateApiUrl($"/tournament/{tournament.Id}"));
       
        // Returned a 200
        Assert.Equal(System.Net.HttpStatusCode.OK, deleteTournamentResponse.StatusCode);
       
        //Confirm deletion
        using HttpResponseMessage confirmDeleteTournamentResponse = await client.GetAsync(CreateApiUrl($"/tournament/{tournament.Id}"));
       
        // Return a 404 showing that the tournament is deleted
        Assert.Equal(System.Net.HttpStatusCode.NotFound, confirmDeleteTournamentResponse.StatusCode);
    }

    private string CreateApiUrl(string endpoint) {
        return $"{baseAPIUrl}{endpoint}";
    }
}