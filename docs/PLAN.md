# Agenda

## Eric - Task 1
1. Create the initial project bootstrap
2. Add one unit test
3. Add a single endpoint for get all tournaments
4. Add one system test

## Jolene - Task 1

Concepts:
- Restful APIs
  - POST, PATCH, DETELE, PUT
  - url parameters

1. Run the code
2. Set up a POSTMAN request
3. Add a new endpoints that includes all tournaments with a specific word in there title
   1. Add, Delete, Query
4. Add a postman method to test the endpoint
5. Add a system test to verify the endpoint works as expected
6. Create a merge request and eric can add feedback

## Jolene - Task 2

Concept:
- Unit tests
  - Thinking about edge cases
  - Motivation
  - Mocks
  - Examples

1. Add an endpoint that returns tournaments matching a keyword
2. Add unit tests to verify all scenarios
   1. Will involve mocking the repository to return a list
3. Include the search as a system test

## Eric - Task 2
- Add basics for entity core
- Dockerfile for the database 
- SQL to create the database and the tournament
  - 
- Implement get functionality
  - 

## Jolene - Task 3

Concepts:
- Docker intro
- Databases

1. Run the docker containers
2. Open up the UI to view the database
3. Run the application to connect to the database
4. Migrate over the remaining unimplemented repository methods to use the database



# Ideas

## Concepts
- Restful APIs
- Dependency Injection
  - Lifecycle
- Unit testing
- CI / CD
- Containers
- System tests
- Not exposing implementation details
- Databases 
- Branching 
- Code reviews
- Exceptions
- Logging
- Hands on coding 
- Debugging
- Bug fixes


Stretch:
- Message system (rabbit MQ)
- Middleware
- Authorization (jwt?)

## Technologies / Tools
- Postman
- Gitlab CI
- Docker
- Postgres
- Git

Stretch:
- Kubernetes 
- Rabbit MQ

## Excercises
- Create a database table
- Set up some dependency injection

## Features
- Round Robin Tournament with teams
  - You can add venues
  - You can add teams
  - You can create a schedule (UT)
  - You can submit game scores 
  - Calculate standings (UT)
  - Determine the winner (UT)
  - Query the schedule

- Basic Auth server that lets you retrieve a jwt

- Notification server that listens to game events coming from rabbitmq and just logs the details  
  - Very basic

