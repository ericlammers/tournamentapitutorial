## Project Structure

### Layered Architecture
A common way to organise APIs in C# is to used a layered architecture. 

In this project for example we have the following 3 layers:
1. Controllers
2. Services
3. Repositories

**Controllers define the public interface of the application.** The public interface for a restful API is the set of endpoints for the API. Controllers should only contain logic related handling requests and generating responses. So for example determining the status code to return is a controllers responsibility but business logic like for example creating a tournament schedule is not part of the job of controllers since it is not request / response related. Right now there is only the one controller with is the `Controllers/TournamentAPI`. It defines a single endpoint which is a `/tournments/{id}` `GET` request that returns a `200` status code (success) with the tournament in the response body if the tournament is found and a `404` status code (not found) with an empty body if the tournament does not exist.

**Services handle the business logic the an application.** The one method in the service right now isn't a good example because it just does a simple call through. But when we add methods like generating a schedule or calculating standings that business specific logic will reside in the service layer.

**Repositories are responsible for retrieving data.** Retrieving data from databases can be complicated. Services already contain the complex business logic so we don't want to muddle that code with data access details. Instead we implement repositories that take care of the data acccess details. They typically have a simple interface with methods like `Get` or `Create` which make it easy for the classes to use them to retrieve data. Right now there is only an in memory version of the repository `Repositories/InMemory/InMemoryTournamentRepository` but eventually we will add a new implementation that actually retrieves data from a database.

`KEY CONCEPT - Do one thing and only one thing:` Well designed software is simple. You want as much as possible for the code to be very stright forward, almost boring. This is true for the layered architecture describe above. Each layer has a specific single responsibility and it should very rarely, if ever, do more then it's one job. When developing always try understand and keep in what the job of a class is. Ask yourself does this logic belong here or is it outside the scope (responsbility) of this class. For example are you adding business logic to the controller, like schedule generation, that's a code smell because we know the controllers job is to only handle request and response logic. If we can keep a clear picture in our minds about what each class should do and if each class only has one job we are in great shape :). 

### Use of interfaces
In C# it is common to define interfaces for almost every class. There are quite a few reasons for this: it makes testing easier (we'll see more on this later), it decouples the code and it helps shield the classes from the complexity of their dependencies.

**The code is decoupled** because the class doesn't need to know about the implemention details of it's dependencies. An example here is if the TouramentService uses a tournament repository but relies only on the ITournamentRepository interface instead of the concrete implementation then it is not tied to any one implementation. We could (and will) start with an in memory repository and later switch to a repository that accesses a database without the service knowing or needing to be updated.

**Shields the class from it's dependencies complexity** very similar to the above point but what this also means is a class doesn't need to worry about the implementation details. It doesn't need to worry about database access details. It only knows about the simple interface and that is all it needs to care about. If we didn't use and interface and instead passed in a concrete class like PostgresTournamentRepository then you might end up with logic in your service related to postgres. But this risk is removed if we just use ITournamentRepository and ignore that it might be implemented behind the scenes with Postgres.

`Clarification:` A classes dependencies are simply the classes it relies on to do it's job. For example if a TournamentService uses the TournamentRepository then the repository is a dependency of the service.