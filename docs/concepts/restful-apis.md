## Restful APIs
Restful APIs are one of the most common APIs used in software development.

### Erics rough notes
- Requests
  - An HTTP request is made up of an http method, endpoint and request body
  - The http method helps communicate the purpose of the request. Example http methods are GET (retrieve data), POST (add data), DELETE (delete data), PUT / PATCH (update data)
  - Endpoint describes what data the request is for, for example /tournaments/{id} is meant to work with a specific tournament with that spefied id
  - Request body contains additional request details, not all http request have bodies, for example GET typically does not have a request body but post often does
- Response 
  - HTTP response contains headers, http status code and a response body
  - Http status codes tell you details about the reponse like did it succeed or not. For example 200s indicate success, 400s indicate client error (like 404 not found), 500s indicate server errors (for example if the database wasn't available so the request failed)
- Restful APIs are stateless, what that means in practise that the API keeps no data about past requests. This isn't to say the APIs don't save data. They do. It's just that each request is look at independently of past requests.
- Typically restful APIs work with json. So the request bodies and response bodies contain json that must be serialised and deserialised

### Key things to understand
- What are the http methods and what is each method used for?
- What are the types of http status codes and what does each mean?
- Any restful best practises that you read about :)

### Research
I'd suggest for this one finding a short youtube video that goes over the basics of restful APIs. Probably this is one where you don't need to go to deep right now. I'd watch a like a 10 minute video at 1.5 speed just to get the jist of it at a high level. As you add endpoints to the API some of this will sink in more and then once you have some practical experience you could maybe read some more.
